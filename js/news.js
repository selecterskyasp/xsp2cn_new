function News(newID,newKeyword){
	this.newID=newID;
	this.newKeyword=newKeyword;
	
	//获取热点新闻
	this.getHotNews=function(){
		$.ajax({
		type:"get",	
		dataType:"html",	
		url:"/common/news/getHotNews.asp",
		beforeSend:function(XMLHttpRequest){
			$("#hotNews").html("<img src='/common/jquery/images/loading.gif' border='0' height='30' width='30' />");
			},
		success:function(data, textStatus){
			$("#hotNews").html(data);
			},
		complete: function(XMLHttpRequest, textStatus){
				//HideLoading();
			},
		error: function(){
				//alert("最热新闻结果可能没有显示");
			}
		
		});
	};
	//获取推荐的产品
	this.getTjProducts=function(){
		$.ajax({
		type:"get",	
		dataType:"html",	
		url:"/common/news/getTjProducts.asp",
		beforeSend:function(XMLHttpRequest){
			$("#prolist2").html("<img src='/common/jquery/images/loading.gif' border='0' height='30' width='30' />");
			},
		success:function(data, textStatus){
			$("#prolist2").html(data);
			},
		complete: function(XMLHttpRequest, textStatus){
				//HideLoading();
			},
		error: function(){
				//alert("最热新闻结果可能没有显示");
			}
		
		});
	};
	//获取相关的信息
	this.getRelativeNews=function(){
		$.ajax({
		type:"get",	
		dataType:"html",	
		url:"/common/news/getRelativeNews.asp?k="+this.newKeyword+"&id="+this.newID,
		beforeSend:function(XMLHttpRequest){
			$("#relative").html("<img src='/common/jquery/images/loading.gif' border='0' height='30' width='30' />");
			},
		success:function(data, textStatus){
			$("#relative").html(data);
			},
		complete: function(XMLHttpRequest, textStatus){
				//HideLoading();
			},
		error: function(){
				//alert("最热新闻结果可能没有显示");
			}
		
		});
	};	
}

//增加鼠标事件
function addMouseover(listid,obj)
{	
	 $("#"+listid+" li").each(function(i){	
					  if(this!=obj)
					  $("#"+this.id+i).hide("fast");
					  else
					  $("#"+this.id+i).show("fast");
			})
}

$(document).ready(function(){						 				  
						   $("#prolist2 li").bind("mouseover",function(){addMouseover("prolist2",this);});	
						   })