<%
'==================================================
'函数名：GetHttpPage
'作  用：获取网页源码
'参  数：HttpUrl ------网页地址
'==================================================
Function GetHttpPage(byVal HttpUrl)
on error resume next
   If IsNull(HttpUrl)=True Or Len(HttpUrl)<18 Or HttpUrl="$False$" Then
      GetHttpPage="$False$"
      Exit Function
   End If
   Dim Http
   Set Http=server.createobject("MSXML2.XMLHTTP")
   Http.open "GET",HttpUrl,False
   Http.Send()
   if err then
      Set Http=Nothing 
      GetHttpPage="$False$"
      Exit function
   end if
   If Http.Readystate<>4 then
      Set Http=Nothing 
      GetHttpPage="$False$"
      Exit function
   End if
   GetHTTPPage=bytesToBSTR(Http.responseBody,"gbk")
   Set Http=Nothing
   If Err.number<>0 then
      Err.Clear
   End If
End Function

'==================================================
'函数名：BytesToBstr
'作  用：将获取的源码转换为中文
'参  数：Body ------要转换的变量
'参  数：Cset ------要转换的类型
'==================================================
Function BytesToBstr(byVal Body,byVal Cset)
   Dim Objstream
   Set Objstream = Server.CreateObject("adodb.stream")
   objstream.Type = 1
   objstream.Mode =3
   objstream.Open
   objstream.Write body
   objstream.Position = 0
   objstream.Type = 2
   objstream.Charset = Cset
   BytesToBstr = objstream.ReadText 
   objstream.Close
   set objstream = nothing
End Function


'==================================================
'函数名：UrlEncoding
'作  用：转换编码
'==================================================
Function UrlEncoding(byVal DataStr)
    Dim StrReturn,Si,ThisChr,InnerCode,Hight8,Low8
    StrReturn = ""
    For Si = 1 To Len(DataStr)
        ThisChr = Mid(DataStr,Si,1)
        If Abs(Asc(ThisChr)) < &HFF Then
            StrReturn = StrReturn & ThisChr
        Else
            InnerCode = Asc(ThisChr)
            If InnerCode < 0 Then
               InnerCode = InnerCode + &H10000
            End If
            Hight8 = (InnerCode  And &HFF00)\ &HFF
            Low8 = InnerCode And &HFF
            StrReturn = StrReturn & "%" & Hex(Hight8) &  "%" & Hex(Low8)
        End If
    Next
    UrlEncoding = StrReturn
End Function

'==================================================
'函数名：GetBody
'作  用：截取字符串
'参  数：ConStr ------将要截取的字符串
'参  数：StartPos ------开始查找位置
'参  数：StartStr ------开始字符串
'参  数：OverStr ------结束字符串
'参  数：IncluL ------是否包含StartStr
'参  数：IncluR ------是否包含OverStr

'==================================================
Function GetBody(byVal ConStr,byVal StartPos_,byVal StartStr,byVal OverStr,byVal IncluL,byVal IncluR)   
   If ConStr="$False$" or ConStr="" or IsNull(ConStr) Or StartStr="" or IsNull(StartStr) Or OverStr="" or IsNull(OverStr) or IsNull(StartPos_) or not isnumeric(StartPos_) or StartPos_<1 Then   	  
      GetBody="$False$|-1"
      Exit Function
   End If
   
   Dim ConStrTemp
   Dim Start,Over
   Dim tempStr
   ConStrTemp=Lcase(ConStr)
   StartStr=Lcase(StartStr)
   OverStr=Lcase(OverStr)
   Start = InStrB(StartPos_, ConStrTemp, StartStr, vbBinaryCompare)
   If Start<=0 then
      GetBody="$False$|-1"
      Exit Function
   Else
      If IncluL=False Then
         Start=Start+LenB(StartStr)
      End If
   End If
   Over=InStrB(Start,ConStrTemp,OverStr,vbBinaryCompare)
   If Over<=0 Or Over<=Start then
      GetBody="$False$|-1"
      Exit Function
   Else
      If IncluR=True Then
         Over=Over+LenB(OverStr)
      End If
   End If
   tempStr=MidB(ConStr,Start,Over-Start)
   tempStr=replace(tempStr,"|","")
   tempStr=tempStr+"|"+cstr(Over+1)
   GetBody=tempStr
End Function

'==================================================
'过程名：SaveRemoteFile
'作  用：保存远程的图片到本地
'参  数：LocalFileName ------ 本地文件名
'参  数：RemoteFileUrl ------ 远程文件URL
'==================================================
Function SaveRemoteFile(byVal LocalFileName,byVal RemoteFileUrl)
on error resume next
    SaveRemoteFile=false
	dim Ads,Retrieval,GetRemoteData	
	'response.Write(server.MapPath(LocalFileName)&"_"&RemoteFileUrl)
'	response.Flush()

	Set Retrieval = Server.CreateObject("Microsoft.XMLHTTP")
	With Retrieval
		.Open "Get", RemoteFileUrl, False, "", ""
		.Send
		if err then
			SaveRemoteFile=False
            Exit Function
		end if
        If .Readystate<>4 or .status<>200 then
            SaveRemoteFile=False
            Exit Function
        End If
		GetRemoteData = .ResponseBody
	End With
	Set Retrieval = Nothing
	Set Ads = Server.CreateObject("Adodb.Stream")
	With Ads
		.Type = 1
		.Open
		.Write GetRemoteData
		.SaveToFile server.MapPath(LocalFileName),2
		if err then
			SaveRemoteFile=False
            Exit Function
		end if
		.Cancel()
		.Close()
	End With
	Set Ads=nothing	
	SaveRemoteFile=true
end Function

'获取扩展名
function GetExt(byVal FileName)
	GetExt = "."&right(FileName,len(FileName)-instrrev(FileName,"."))	
	FileName=""	
end function

'==================================================
'函数名：PostHttpPage
'作　用：登录
'参　数：RefererUr－－－－－登录地址
'参　数：PostUrl－－－－－提交地址
'参　数：PostData－－－－－用户参数
'==================================================
Function PostHttpPage(RefererUrl,PostUrl,PostData) 
    Dim xmlHttp 
    Dim RetStr      
    Set xmlHttp = CreateObject("Msx" & "ml2.XM" & "LHT" & "TP")  
    xmlHttp.Open "POST", PostUrl, False
    XmlHTTP.setRequestHeader "Content-Length",Len(PostData) 
    xmlHttp.setRequestHeader "Content-Type", "application/x-www-form-urlencoded"
    xmlHttp.setRequestHeader "Referer", RefererUrl
    xmlHttp.Send PostData 
    If Err.Number <> 0 Then 
        Set xmlHttp=Nothing
        PostHttpPage = "$False$"
        Exit Function
    End If
    PostHttpPage=bytesToBSTR(xmlHttp.responseBody,"GB2312")
    Set xmlHttp = nothing
End Function 
%>