<!--#include virtual="/inc/top.asp"-->
<!--#include file="../inc/nostylepage.asp"-->
<!--#include file="mhelpsub.asp"-->
<%
id=request.QueryString("id")
if not chkrequest(id) then id=24 else id=clng(id)
bidname=getcurNamelist(id)
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta http-equiv="Content-Language" content="UTF-8" />
<meta name="robots" content="all" />
<meta http-equiv="imagetoolbar" content="false" />
<meta name="MSSmartTagsPreventParsing" content="true" />
<link rel="shortcut icon" href="/favicon.ico" type="image/x-icon" />
<meta name="Copyright" content="Copyright (c) 2009 www.intltj.com" />
<meta name="Keywords" content="Yiwu Tujin Co., Ltd." />
<meta name="Description" content="Yiwu Tujin Co., Ltd." />
<link rel="stylesheet" href="style/common.css" type="text/css" media="all" />
<title>在线帮助</title>
</head>
<body>

<!--header begin-->
<div id="head">
<div class="company">
<div class="HeadMenu">
<ul>
<li><h3><a href="help.asp">帮助首页</a></h3></li>
</ul>
</div>
</div>
</div>
<!--header end-->

<!--content begin-->
<div id="content">
<!--guidance-->
<div class="guidance">
您现在的位置：<a href="help.asp">帮助首页</a> >> <a href="help.asp?id=<%=id%>"><%=bidname%></a> >> <strong>列表信息</strong>
</div>

<div id="ContentLeft">
<ul class="MenuBar">
<span class="title">在线帮助</span>
   <ul class="help">
    <%=getmhelpClass()%>
   </ul>
</ul>
<ul class="MenuBar">
<span class="title">我要搜索</span>
   <ul class="help">
   <form action="help.asp" method="get">
 	<input type="text" name="keyword" style="width:80px" id="keyword" />&nbsp;<input type="submit" value="搜索帮助" />
   </form>
   </ul>
</ul>
</div>
<div id="ContentRight">
<div class="RightTitleBg">
<div class="RightTitle">
<h2><%=getcurNameList(id)%>列表</h2>
</div>
</div>
<div id="RightContent">
<div class="ColumnContent">
   <ul class="help">
   <%
   k=checkstr(request.QueryString("keyword"))  
   st=""
   if chkRange(k,1,50) then st=st&" and charindex('"&k&"',[name])>0"
   st=st&" and bid="&clng(id)
   dim pagesize,pagecount,recordcount,page
	PageSize=15   
	'获得总数  
	sql="select count(id) from mhelp where flag=1"&st
	'response.Write(sql)
	'response.End()
	recordcount=getDbValue(sql,1)(1)
	'总页数  
	if cint(recordcount) = 0 then 
		pagecount=1
	else
		pagecount=Abs(Int(recordcount/PageSize*(-1)))   
	end if
	'获得当前页码   
	page=request.QueryString("page")
	if not chkrequest(page) then page=1 else page=cint(page)
	if page>pagecount then page=pagecount
	response.Write(getmhelpList(pagesize,st))
   %>   
   </ul>
   <%=showpage(pagecount,pagesize,page,recordcount,15)%>
 <!--  <p class="next">
	<strong>12</strong> 条/页&nbsp;&nbsp;共: <strong class="red">3000</strong> 条 &nbsp;共: <strong class="red">200</strong> 页 <span class="disabled">首页</span><span class="current">1</span><a href="#?page=2">2</a><a href="#?page=3">3</a><a href="#?page=4">4</a><a href="#?page=5">5</a>...<a href="#?page=199">199</a><a href="#?page=200">200</a><a href="#?page=2">尾页 </a>
   </p>-->
</div>
</div>

</div>
</div>
<!--content end-->

</body>
</html>