﻿<!--#include file="../top.asp"-->
<!--#include file="updatetime.asp"-->
<!--#include file="../updateSub.asp"-->
<!--#include file="../../inc/nostylepage.asp"-->
<!--#include file="../../products/productSub.asp"-->
<%
tqxarr=getqx(15,qxarr)
if tqxarr(1)<>1 then showqxmsg
'if isxsp2cn then
'	response.Write("总库不需要更新")
'	response.End()
'end if
call closeconn()
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link href="../style/css.css" rel="stylesheet" type="text/css" />
<script language="javascript" src="<%=webvirtual%>/js/jquery.js"></script>
<script language="javascript" src="<%=webvirtual%>/js/function.js"></script>
<title></title>
<style>
.curred{color:#FF0000}
</style>
</head>
<body>
<table border="0" cellspacing="0" cellpadding="0" class="MsgTbl">
  <tr>
    <td width="52%" class="tbTitle">后台 > <a href="list.asp">产品管理</a> > 同步产品分类</td>    
  </tr>
</table>

<table border="0" cellspacing="0" cellpadding="0" class="MsgTbl"> 
 <tr>
 	<td  align="left">
	<span style="padding-left:15px;  color:#FF0000">
	<img src="<%=webvirtual%>/jquery/images/warning.gif" />警告：同步产品的分类会覆盖您对产品分类做的所有的修改，它会自动同步产品分类、颜色、价格分类等与产品相关的所有的信息，此操作有一定的危险性，请慎重！如果出现异常或者丢失信息请不要重复操作，并及时联系我们！特别注意：同步产品分类后，可能会导致您不能添加新的产品分类，这时必须联系我们帮您解决	</span>	</td>
 </tr> 
 <tr>
 	<td  align="center">
	<input type="button" value="立即开始同步产品分类" onclick="updateclass()" />&nbsp;<span style="padding-left:15px; display:none; color:#FF0000" id="updateclass">
	<img src="<%=webvirtual%>/jquery/images/loading2.gif" />正在同步，请稍后。。。
	</span>	</td>
 </tr>  
</table>
<script language="javascript">
function updateclass()
{
	$.ajax({
	type:"get",	
	dataType:"html",	
	url:"updateclass.asp?r="+Math.random()*100,	
	beforeSend:function(XMLHttpRequest){
		$("#updateclass").css("display","");
		},
	success:function(data, textStatus){		
		$("#updateclass").html(data);
		},
	complete: function(XMLHttpRequest, textStatus){
			//HideLoading();
		},
	error: function(){
			alert("脚本错误，请与我们联系");
		}
	
	});
	return false;
}
</script>
</body>
</html>
