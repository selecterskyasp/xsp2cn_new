﻿<!--#include file="../top.asp"-->

<%
tqxarr=getqx(23,qxarr)
if tqxarr(0)<>1 then showqxmsg
id=request.QueryString("id")
if not chkrequest(id) then
	alert "出库信息不存在","",1
end if
set rs=server.CreateObject("adodb.recordset")
rs.open "select top 1 * from [chuku] where id="&id,conn,1,1
%>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>出库详细</title>
<link rel="stylesheet" type="text/css" href="../style/css.css">
<script language="javascript" src="<%=webvirtual%>/js/validator.js"></script>
</head>

<body>
<table  border="0" cellspacing="1" cellpadding="1" width="100%" bordercolorlight="#cccccc" bordercolordark="#FFFFFF">
  <tr>
    <td height=30 align="left" colspan="2" bgcolor="#f7f7f7" class="membertr" ><font color=#666666 size="+1"> 详细信息</font></td>
  </tr>
  <tr>
    <td height=30 width="21%" align=right>产品信息： &nbsp;</td>
	<td width="79%" align="left">产品名称：<span class="yellow"><%=rs("pname")%></span>&nbsp;产品编号：<span class="yellow"><%=rs("phh")%></span></td>
  </tr>
  <tr>
    <td height=30 align=right>价格信息： &nbsp;</td>
	<td align="left">普通价：<span class="yellow"><%=getprice(rs("pprice"),2)%></span>元 市场价：<span class="yellow"><%=getprice(rs("pprice1"),2)%></span>元 成本价：<span class="yellow"><%=getprice(rs("pprice2"),2)%></span>元</td>
  </tr> 
  
  <tr>
    <td height=30 align=right>积分信息： &nbsp;</td>
	<td align="left">购买此产品所需积分：<span class="yellow"><%=rs("pjifen")%></span>点 购买成功产生积分：<span class="yellow"><%=rs("jf")%></span>点</td>
  </tr>  
  <tr>
    <td height=30 align=right>出库信息： &nbsp;</td>
	<td align="left">
订单号：<a class="yellow" href="../dingdan/dingdandetail.asp?id=<%=rs("number")%>"><%=rs("number")%></a>&nbsp;<a href="../yp/detail.asp?id=<%=rs("userid")%>">查看用户详细</a>	
    </td>
  </tr> 
 <tr>
    <td height=30 align=right>出库时间： &nbsp;</td>
	<td align="left"><%=rs("createdon")%></td>
  </tr>  
  <tr>
    <td height=30 align="center" colspan="2"><a href="javascript:;" onclick="history.back();return false;">返回</a></td>
  </tr>
<%
rs.close
set rs=nothing
closeconn
%>
</table>
</body>
</html>
