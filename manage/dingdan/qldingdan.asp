<!--#include file="../top.asp"-->
<%
tqxarr=getqx(16,qxarr)
if tqxarr(1)<>1 then showqxmsg
id=request.QueryString("id")
if not chkrequest(id) then alert "error","",1
set rs=server.CreateObject("adodb.recordset")
rs.open "select top 1 flag,flagbak,tdmemo from dingdan where id="&id&"",conn,1,1
if rs.bof and rs.eof then
call showInfo("操作失败","javascript:history.back()","找不能此定单,可能该定单已经删除")
response.End()
end if
tdmemo=rs("tdmemo")
flag=cint(rs("flag"))
flagbak=rs("flagbak")
'当前订单状态不是 申请退单
if flag<>5 then
	call showInfo("操作失败","javascript:history.back()","当前订单状态不允许此操作")
	response.End()
end if

if request.QueryString("act")="save" then	
	
	f_flag=request.Form("f_flag")
	f_memo=checkstr(request.Form("f_memo"))
	tuidanmethod=request.Form("tuidanmethod")
	if (f_flag<>"6" and f_flag<>"7") or not chkRange(f_memo,2,300) or (tuidanmethod<>"0" and tuidanmethod<>"1")  then
		call showInfo("操作失败","javascript:history.back()","请填写必要的信息")
		response.End()
	end if
	'这里可以存放如果成功退单将对产品库的库存,用户表的积分进行相应逆操作的代码
	'同意退单
	f_flag=cint(f_flag)
	tuidanmethod=cint(tuidanmethod)	
		set comm=server.CreateObject("adodb.command")
		with comm
		.activeconnection=conn
		.commandType=4
		.commandText="tuidan"
		.prepared=true
		.parameters.append .createparameter("@return",3,4)
		.parameters.append .createparameter("@id",20,1,8,id)
		.parameters.append .createparameter("@f_flag",16,1,2,f_flag)
		.parameters.append .createparameter("@tuidanmethod",11,1,1,tuidanmethod)
		.parameters.append .createparameter("@tdmemo",200,1,300,f_memo)
		.execute()
		end with
		yesno=cint(comm(0))
		set comm=nothing
		dim tarr(6)
		tarr(0)="success"
		tarr(1)="订单不存在"	
		tarr(2)="订单当前交易状态不是 申请退单"
		tarr(3)="用户不存在"
		tarr(4)="增加积分消费记录失败"
		tarr(5)="增加预存款消费记录失败"
		tarr(6)="还原产品库存，增加库存记录失败"
		if yesno<>0 then
			call showInfo("操作失败","javascript:history.back()","错误ID号："&yesno&",msg:"&tarr(yesno))
		else
			if f_flag=6 then
				call showInfo("操作成功","dingdandetail.asp?id="&id,"已经退单")
			else
				call showInfo("操作成功","dingdandetail.asp?id="&id,"已经拒绝退单")
			end if
		end if
		erase tarr
end if

call closers(rs)
call closeconn()
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>无标题文档</title>
</head>

<body>
<link rel="stylesheet" href="../style/css.css" type="text/css" media="all" />
<script language="javascript" src="<%=webvirtual%>/js/validator.js"></script>
<form action="?act=save&id=<%=id%>" method="post" onSubmit="return Validator.Validate(this,2)">  
<table  border="0" cellspacing="0" cellpadding="0" width="100%">
  <tr>
    <td height=30 align="left" colspan="2" bgcolor="#f7f7f7" class="membertr" ><font color=#666666 size="+1">处理退单</font></td>
  </tr>  
    <tr>
    <td height=30 align=right width="43%">处理结果： &nbsp;</td>
    <td width="57%" align="left"><label>
      <input type="radio" name="f_flag" value="6" onclick="document.getElementById('sptuidanmethod').style.display='';" />
      同意退单 
      <input name="f_flag" type="radio" value="7" checked="checked" onclick="document.getElementById('sptuidanmethod').style.display='none';"/>
    拒绝退单</label></td>
  </tr> 
   <tr id="sptuidanmethod" style="display:none">
    <td height=30 align=right width="43%">退单方式： &nbsp;</td>
    <td width="57%" align="left"><label>
      <input type="radio" name="tuidanmethod" value="0" />
      现金退款 
      <input name="tuidanmethod" type="radio" value="1" checked="checked" />
    退款至预存款</label></td>
  </tr> 
     <tr>
    <td height=30 align=center colspan="2">
   <textarea name="f_memo" cols="50" rows="10" id="f_memo" dataType="Require" msg="请填写退单原因"><%=tdmemo%></textarea>
   这里填写原因</td>
  </tr>
   
  <tr>
    <td height=30 align="center" colspan="2"><label>
      <input type="submit" name="Submit" value="提交" onclick="return confirm('确认操作吗?');">&nbsp; 
      <input type="button" name="Submit2" value="返回" onClick="javascript:history.back()">&nbsp;</label></td>
  </tr>
</table>
</form>  
</body>
</html>