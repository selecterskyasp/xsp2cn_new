<!--#include file="../top.asp"-->
<%
tqxarr=getqx(16,qxarr)
if tqxarr(1)<>1 then showqxmsg
id=request.QueryString("id")
if not chkrequest(id) then alert "error","",1
set rs=server.CreateObject("adodb.recordset")
rs.open "select top 1 flag,jjmemo from dingdan where id="&id&"",conn,1,1
if rs.bof and rs.eof then
call showInfo("操作失败","javascript:history.back()","找不能此定单,可能该定单已经删除")
response.End()
end if
flag=cint(rs("flag"))
jjmemo=rs("jjmemo")
'订单状态必须为 已经付款 付款未确认 付款已经确认,正在配货
if flag<>2 and flag<>33  and flag<>44 then
	call showInfo("操作失败","javascript:history.back()","当前订单状态不允许此操作")	
	response.End()
end if

if request.QueryString("act")="save" then
	
	f_flag=request.Form("f_flag")
	f_memo=checkstr(request.Form("f_memo"))		
	if (f_flag<>"3" and f_flag<>"33" and f_flag<>"44") or not chkRange(f_memo,2,300)  then
		call showInfo("操作失败","javascript:history.back()","请填写必要的信息")
		response.End()
	end if
	
	f_flag=cint(f_flag)	
		set comm=server.CreateObject("adodb.command")
		with comm
		.activeconnection=conn
		.commandType=4
		.commandText="fahuo"
		.prepared=true
		.parameters.append .createparameter("@return",16,4)
		.parameters.append .createparameter("@id",20,1,8,id)
		.parameters.append .createparameter("@f_flag",16,1,2,f_flag)
		.parameters.append .createparameter("@tdmemo",200,1,300,f_memo)
		.parameters.append .createparameter("@adduser",200,1,20,user)
		.parameters.append .createparameter("@err",200,2,600)
		.execute()
		end with
	
		yesno=cint(comm(0))
		dim errmsg
		errmsg=comm("@err")
		dim errtarr(7)
		errtarr(0)="success"
		errtarr(1)="订单不存在"	
		errtarr(2)="定单状态不是 已经付款 已经发货 付款未确认 付款已经确认"
		errtarr(3)="至少有一个产品不存在"
		errtarr(4)="库存不足"
		errtarr(5)="订购产品数量少于最小起订量"
		errtarr(6)="订购产品数量多于最大供应量，可能是产品库存不足或产品不存在引起"
		errtarr(7)="加入出库表失败"
		set comm=nothing
		if yesno<>0 then
			response.Write("<br><font color=red>操作失败,产生错误的产品列表：<h1>"&errmsg&"</h1>可能原因："&errtarr(yesno)&"</font><br><a href=""dingdandetail.asp?id="&id&""">返回订单详细页</a>&nbsp;<a href=""../product/list.asp"">返回产品列表页</a>&nbsp;<a href=""dingdan.asp"">返回订单列表页</a>&nbsp;<a href=""javascript:history.back();"">返回上一页</a>")
			response.End()			
		else
			select case f_flag
			case 3
				call showInfo("操作成功","dingdandetail.asp?id="&id,"已经发货!")
			case 33
				call showInfo("操作成功","dingdandetail.asp?id="&id,"当前订单状态：付款未确认")
			case 44
				call showInfo("操作成功","dingdandetail.asp?id="&id,"付款已经确认，正在配货!")
			end select
		end if
		erase errtarr
end if

call closers(rs)
call closeconn()
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>无标题文档</title>
</head>

<body>
<link rel="stylesheet" href="../style/css.css" type="text/css" media="all" />
<script language="javascript" src="<%=webvirtual%>/js/validator.js"></script>
<form action="?act=save&id=<%=id%>" method="post" onSubmit="return Validator.Validate(this,2)">  
<table  border="0" cellspacing="0" cellpadding="0" width="100%">
  <tr>
    <td height=30 align="left" colspan="2" bgcolor="#f7f7f7" class="membertr" ><font color=#666666 size="+1">处理发货</font></td>
  </tr>  
    <tr>
    <td height=30 align=right width="43%">处理结果： &nbsp;</td>
    <td width="57%" align="left"><label>
	<input name="f_flag" type="radio" value="33" <%if flag=33 then response.Write("checked")%> <%if flag=3 then response.Write("disabled")%>/>
      未收到款
      <input name="f_flag" type="radio" value="44" <%if flag=44 then response.Write("checked")%> <%if flag=3 then response.Write("disabled")%>/>
      已收到款，正在配货
      <input type="radio" name="f_flag" value="3" <%if flag=3 then response.Write("checked disabled")%>/>
      已发货       
    </label></td>
  </tr> 
     <tr>
    <td height=30 align=center colspan="2">
   <textarea name="f_memo" cols="50" rows="10" id="f_memo" dataType="Require" msg="请填原因"><%=jjmemo%></textarea>
   说明备注 300以内 </td>
  </tr>
   
  <tr>
    <td height=30 align="center" colspan="2"><label>
      <input type="submit" name="Submit" value="提交" onclick="return confirm('确认操作吗?');">&nbsp; 
      <input type="button" name="Submit2" value="返回" onClick="javascript:history.back()">&nbsp;</label></td>
  </tr>
</table>
</form>  
</body>
</html>