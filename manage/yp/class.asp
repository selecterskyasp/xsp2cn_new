﻿<!--#include file="../top.asp"-->
<%
tqxarr=getqx(12,qxarr)
if tqxarr(0)<>1 then showqxmsg
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>分类</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<link rel="stylesheet" type="text/css" href="../style/css.css">
<script language="javascript" src="<%=webvirtual%>/js/validator.js"></script>
</head>
<body>
<table width="90%" height="50" border="0" align="center" cellpadding="5" cellspacing="1" bgcolor="#cccccc">
<tr bgcolor="#efefef">
    <td align="left">
	管理后台 - > <a href="class.asp">会员等级管理</a>
	</td>
</tr>
</table>
<%
sql="select id,[name],zheke,qipi,jifen,[money],yunfei from userflag order by adddate desc"
tarr=getDBvalueList(sql)
%>
<table width="90%" height="0" border="0" align="center" cellpadding="5" cellspacing="1" bgcolor="#cccccc">
<tr bgcolor="#ffffff">
    <td align="center">会员ID</td>
	<td align="center">会员名称</td>
	<td align="center">起批金额</td>
	<td align="center">操作</td>
</tr>
<%
if clng(tarr(0,0))<>0 then 
	dim tempid,nextcount,ttarr,tparentid,prelink
	for i=0 to ubound(tarr,2)
		tempid=tarr(0,i)
		%>
<tr bgcolor="#ffffff">
    <td align="center"><%=tempid%></td>
	<td align="center"><%=tarr(1,i)%></td>
	<td align="center"><%=tarr(3,i)%></td>
	<td align="center"><a href="class_save.asp?id=<%=tempid%>&myAction=deleteSort" onclick="return confirm('该操作将会删除属于该等级的会员和产品价格信息不能恢复，确认吗？');">删除</a> | <a href="class.asp?pc=editSort&id=<%=tempid%>">修改</a> | <a href="class_save.asp?id=<%=tempid%>&myAction=sort">排序</a></td>
</tr>
<%		
	next
end if
erase tarr
%>
<tr bgcolor="#efefef">
    <td colspan="4" align="right"><a href="class.asp?pc=addSort">添加</a> | <a href="javascript:history.back();">返回</a></td>
</tr>
</table>
<%
dim pc
pc=request.QueryString("pc")' 类别操作的动作
if pc<>"" then 
	if tqxarr(1)<>1 then showqxmsg
	id=request.QueryString("id")
	dim sortName,zheke,qipi,jifen,money,yunfei
	sortName="":zheke=1:qipi=50:jifen=0:money=0:yunfei=0
	if pc="editSort" then
		sql="select [name],zheke,qipi,jifen,[money],yunfei from userflag where id="&id
		tarr=getdbvalue(sql,6)
		if tarr(0)=0 then
			alert "分类不存在","",1
		end if
		sortName=tarr(1):zheke=getprice(tarr(2),2):qipi=tarr(3):jifen=tarr(4):money=tarr(5):yunfei=tarr(6)
		erase tarr
	end if
%>
<table width="90%" border="0" align="center" cellpadding="3" cellspacing="1" bgcolor="#cccccc">
   <form action="class_save.asp" method="post" name="add" onSubmit="return Validator.Validate(this,2);">
		<input name="myAction" type="hidden"  value="<%=pc%>">		
		<input type="hidden" name="id" value="<%=id%>">
   <tr> 
	   <td width="20%" align="right" bgcolor="#FFFFFF">会员名称：</td>
      <td align="left" bgcolor="#FFFFFF"><input name="Name" type="text" value="<%=sortName%>" dataType="LimitB" min="2" max="10" msg="会员名称必须为2-10个汉字以内"></td>
   </tr>
     <tr> 
	   <td width="20%" align="right" bgcolor="#FFFFFF">会员折扣：</td>
      <td align="left" bgcolor="#FFFFFF"><input name="zheke" type="text" value="<%=zheke%>" size="10" dataType="Double" msg="会员折扣必须是大于0小于等于1的数字">
      折
       <span class="red">折扣只作为参考，不一定是购买产品的折扣价格</span></td>
   </tr>
    <tr> 
	   <td width="20%" align="right" bgcolor="#FFFFFF">最小起批：</td>
      <td align="left" bgcolor="#FFFFFF"><input name="qipi" type="text" value="<%=qipi%>" size="20" dataType="Number" msg="会员折扣必须是大于0小于等于1的数字">
      元
      <span class="red">会员下定单最少要达到的金额</span></td>
   </tr> 
    <tr> 
	   <td width="20%" align="right" bgcolor="#FFFFFF">所需积分：</td>
      <td align="left" bgcolor="#FFFFFF"><input name="jifen" type="text" value="<%=jifen%>" size="20" dataType="Integer" msg="所需积分必须是大于等于0的整数">
      分
      <span class="red">会员自动升级时所需要的积分</span></td>
   </tr> 
    <tr> 
	   <td width="20%" align="right" bgcolor="#FFFFFF">所需金额：</td>
      <td align="left" bgcolor="#FFFFFF"><input name="money" type="text" value="<%=money%>" size="20" dataType="Integer" msg="所需金额必须是大于等于0的整数">
      元
      <span class="red">会员自动升级时所需要的消费总金额</span></td>
   </tr> 
   <tr> 
	   <td width="20%" align="right" bgcolor="#FFFFFF">运费金额：</td>
      <td align="left" bgcolor="#FFFFFF"><input name="yunfei" type="text" value="<%=yunfei%>" size="20" dataType="Integer" msg="运费金额必须是大于等于0的整数">
      元
      <span class="red">订单金额达到多少是免运费 为0时不免</span></td>
   </tr>
   <tr> 
	   <td align="right" bgcolor="#FFFFFF">&nbsp;</td>
	   <td bgcolor="#FFFFFF">
			<input name="add" type="submit"  value="提交">
			<input type="reset" name="Submit2" value="重置">
		</td>
   </tr>
   </form>
</table>
<%
end if

call closeconn()
%>
</body>
</html>