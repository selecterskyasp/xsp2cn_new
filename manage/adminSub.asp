<%
function showmsg(byVal msg)
	response.Write("<script>alert('您当前操作结果:\n\n"&msg&"');history.back();</script>")
	call closeconn()
	response.End()
end function

sub insert_record(byVal table,byVal Parameters,byVal values)'表名,条件,返回路径
'response.Write("insert into "&table&"("&Parameters&")values("&values&")")
'response.End()
	sql="insert into "&table&"("&Parameters&")values("&values&")"
	conn.execute(sql)
end sub

'sub del_record(byVal table,byVal Conditions)'表名,条件,返回路径
''	response.Write("delete from "&table&" where "&Conditions&"")
''response.End()
'	conn.execute("delete from "&table&" where "&Conditions&"")
'end sub

'生成pagetop.js
function createPagetopJS()
dim body,tmparr
body=getcache("js/pagetop.js")
body=setcommon(body)
tmparr=SaveToFileauto(body,"/js/pagetop.js","utf-8")
if tmparr(0)<>0 then
	call alert("保存pagetop.js文件出错，错误ID号："&tmparr(0)&" 错误信息："&tmparr(1)&"","",1)
end if
erase tmparr:body=""
end function

'生成pagehead.js
function createPageheadJS()
dim body,tmparr
body=getcache("js/pagehead.js")
body=setcommon(body)
tmparr=SaveToFileauto(body,"/js/pagehead.js","utf-8")
if tmparr(0)<>0 then
	call alert("保存pagehead.js文件出错，错误ID号："&tmparr(0)&" 错误信息："&tmparr(1)&"","",1)
end if
erase tmparr:body=""
end function

'生成pagemenu.js
function createPagemenuJS()
dim tarr,tcontent,i,body
dim target
sql="select top 11 id,name,url,target from lanmu where flag=1 order by adddate desc"
tarr=getdbvaluelist(sql)
tcontent=""
if clng(tarr(0,0))<>0 then	
	for i=0 to ubound(tarr,2)		
		if tarr(3,i) then target=" target=""_blank""" else target=""
		tcontent=tcontent&"'<li><h2><a href="""&tarr(2,i)&""" "&target&">"&tarr(1,i)&"</a></h2></li> ' +" & vbcrlf 
	next
end if
body=getcache("js/pagemenu.js")
body=setcommon(body)
body=replace(body,"$lanmu$",tcontent)
tcontent=""
tarr=SaveToFileauto(body,"/js/pagemenu.js","utf-8")
if tarr(0)<>0 then
	call alert("保存pagemenu.js文件出错，错误ID号："&tarr(0)&" 错误信息："&tarr(1)&"","",1)
end if
erase tarr:body=""
end function

'获取产品分类 tbid大分类的ID号 如果为0则默认3864
function createClassJs(tbid)
dim tarr,i,tarr2,j
dim tid,tt,turl,tcontent
const pclassid=3864
if not chkrequest(tbid) then tbid=pclassid
sql="select id,[name] from class where parentid="&tbid&" order by sortid desc"
tarr=getdbvaluelist(sql)
tcontent="document.write("
if clng(tarr(0,0))<>0 then	
	for i=0 to ubound(tarr,2)
		tid=tarr(0,i):tt=tarr(1,i)
		if tbid<>pclassid then turl="/products/?id="&tbid&","&tid else turl="/products/?id="&tid
		tcontent=tcontent&"'<li>'+" & vbcrlf 	
		tcontent=tcontent&"'<h2><div class=""CateTitle""  onclick=""FlClick(\'bid"&tid&"\')"" title=""单击展开/隐藏小分类""><a href="""&turl&""">"&tt&"</a></div></h2>'+"
		tcontent=tcontent&"'<div class=""CateList"" id=""bid"&tid&""">'+" & vbcrlf 
		tcontent=tcontent&"'"
		sql="select id,[name] from class where parentid="&tid&" order by sortid desc"
		tarr2=getdbvaluelist(sql)
		if clng(tarr2(0,0))<>0 then			
			for j=0 to ubound(tarr2,2)
				tt=tarr2(1,j)
				if tbid<>pclassid then turl="/products/?id="&tbid&","&tid&","&tarr2(0,j) else turl="/products/?id="&tid&","&tarr2(0,j)				
					tcontent=tcontent&"<p><a href="""&turl&""">"&tt&"</a></p>"	
			next
			tcontent=tcontent&"' +"&vbcrlf	
			tcontent=tcontent&"'</div></li>'+" & vbcrlf 
		else
		tcontent=tcontent&"</div></li>'+" & vbcrlf 		
		end if
		erase tarr2
		
	next
end if
tcontent=tcontent&"'');"&vbcrlf
createClassJs=savetofileauto(tcontent,"/js/detailfl.js","utf-8")
tcontent="":erase tarr:sql=""
end function

'生成pageSearch.js 和 keyword.js
function createPagesearchKeywordJS()
dim tarr,tcontent,i,target
dim tbody
sql="select id,name,url,target from hotlabel where flag=1 order by adddate desc"
tarr=getdbvaluelist(sql)
tcontent=tcontent&"document.write(" & vbcrlf 
tcontent=tcontent&"'热门标签：' +" & vbcrlf 
if clng(tarr(0,0))<>0 then
	for i=0 to ubound(tarr,2)		
		if tarr(3,i) then target=" target=""_blank""" else target=""
		tcontent=tcontent&"'<a href="""&tarr(2,i)&""" "&target&">"&tarr(1,i)&"</a>&nbsp;' +" & vbcrlf 
	next
end if

tcontent=tcontent&"'');" & vbcrlf 
erase tarr
tbody=getcache("js/pageSearch.js")
tbody=setcommon(tbody)
tbody=replace(tbody,"$keyword$",tcontent)

tarr=SaveToFileauto(tcontent,"/js/keyword.js","utf-8")
if tarr(0)<>0 then
call alert("保存keyword.js文件出错，错误ID号："&tarr(0)&" 错误信息："&tarr(1)&" 当前的操作没有保存","",1)
end if
erase tarr
	
tarr=SaveToFileauto(tbody,"/js/pageSearch.js","utf-8")
if tarr(0)<>0 then
call alert("保存pageSearch.js文件出错，错误ID号："&tarr(0)&" 错误信息："&tarr(1)&" 当前的操作没有保存","",1)
end if
erase tarr	
end function
%>
