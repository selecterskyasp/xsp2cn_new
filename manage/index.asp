<!--#include file="top.asp"-->
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta http-equiv="Content-Language" content="UTF-8" />
<meta name="robots" content="all" />
<meta http-equiv="imagetoolbar" content="false" />
<meta name="MSSmartTagsPreventParsing" content="true" />
<link rel="shortcut icon" href="/favicon.ico" type="image/x-icon" />
<meta name="Copyright" content="Copyright (c) 2009 www.intltj.com" />
<meta name="Keywords" content="Yiwu Tujin Co., Ltd." />
<meta name="Description" content="Yiwu Tujin Co., Ltd." />
<link rel="stylesheet" href="style/common.css" type="text/css" media="all" />
<title>Content Management System</title>
</head>
<body scroll="no">

<!--header begin-->
<div id="head">
<div class="company">
<div class="HeadMenu">
<ul>
<li><h3><a href="./">首页</a></h3></li>
<li><h3><a href="http://www.xsp2.cn/common/manage/help.asp" target="_blank">帮助中心</a></h3></li>
<li><h3><a href="logout.asp">退出后台</a></h3></li>
</ul>
</div>
</div>
</div>
<!--header end-->

<!--content begin-->
<div id="content">
<div id="ContentLeft">
<iframe name="iframeleft" frameBorder="0" frameSpacing="0" height="500" width="221" marginHeight="0" marginWidth="0" scrolling="auto" src="left.asp" style="overflow-x:hidden"></iframe>
</div>
<div id="ContentRight">
<div class="RightTitleBg">
<div class="RightTitle"><span style="float:left">欢迎您！<strong><%=user%></strong></span><span style="float:left; margin-left:5px"></span>
</div>
</div>
<iframe name="iframeright" id="iframeright" frameBorder="0" frameSpacing="0" height="476" width="100%" marginHeight="0" marginWidth="0" scrolling="auto" src="welcome.asp"></iframe>
</div>
</div>

<!--content end-->
<%closeconn%>
</body>
</html>